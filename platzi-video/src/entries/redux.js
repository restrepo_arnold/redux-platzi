import { createStore } from "redux";
const $form = document.getElementById("js-form");

const handleSubmit = e => {
  e.preventDefault();
  const data = new FormData($form);
  const title = data.get("title");
  /**
   * Dispatch
   */
  store.dispatch({
    type: "ADD_SONG",
    payload: { title }
  });
};

$form.addEventListener("submit", handleSubmit);

/**
 * Initial State Redux
 */
const initialState = [
  {
    title: "Despacito"
  }
];

/**
 * Create Reducer
 */

const reducer = function(state, action) {
  switch (action.type) {
    case "ADD_SONG":
      return [...state, action.payload];
    default:
      return state;
  }
};

/**
 * Initial Config in Redux
 */
const store = createStore(
  reducer,
  initialState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

function render() {
  const $playlistContainer = document.getElementById("js-playlist");
  const playlist = store.getState();
  $playlistContainer.innerHTML = "";
  playlist.forEach(item => {
    const templateItem = document.createElement("p");
    templateItem.textContent = item.title;
    $playlistContainer.appendChild(templateItem);
  });
}
render();

/**
 * Suscribe
 */

function handleChange() {
  render();
}

store.subscribe(handleChange);
