import React from "react";
import { render } from "react-dom";
import Home from "../pages/containers/home";
// import Playlist from './src/playlist/components/playlist';
import data from "../api.json";
import { Provider } from "react-redux";
import { createStore } from "redux";
import dataReducer from "../reducers/data";

const initialState = {
  data: {
    ...data
  },
  search: []
};

const store = createStore(
  dataReducer, // reducer
  initialState, // Initial State
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() // Enhancer
);

console.log(store.getState());

const homeContainer = document.getElementById("home-container");

render(
  <Provider store={store}>
    <Home />
  </Provider>,
  homeContainer
);
