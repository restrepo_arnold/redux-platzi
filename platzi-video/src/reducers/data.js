function dataReducer(state, { type, payload }) {
  switch (type) {
    case "SEARCH_VIDEO":
      let results = [];
      if (payload.query) {
        const categories = state.data.categories;
        categories.map(category => {
          let tempResults = category.playlist.filter(item => {
            return item.author
              .toLowerCase()
              .includes(payload.query.toLowerCase());
          });
          results = results.concat(tempResults);
        });
      }
      {
        return {
          ...state,
          search: results
        };
      }
    default:
      return state;
  }
}

export default dataReducer;
