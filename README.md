# Redux Course Platzi

Contenedor predecible para contenedores [Ver Presentación](https://static.platzi.com/media/public/uploads/redux_78ace4a3-2843-4f4d-8816-678a19f50b0d.pdf)

## Motivación

El frontend es muy complejo

## Store

El centro y la verdad de todo, con métodos para actualizar, obtener y escuchar datos.

## Actions

Las acciones son un bloque de información que envía datos desde la aplicación hacia el store.

## Reducers

Cambian el estado de la aplicación

**State => UI => Actions => Reducer => Store => State => UI**

## 3 Principios Fundamentales:

- Única fuente de la verdad - Single Source of Truth
- El Estado es de Sola Lectura - State Is Read Only
- Los cambios se realizan **con funciones puras** - Changes are made with pure functions

1. Única fuente de verdad:
   El estado de toda tu aplicación esta almacenado en un árbol guardado en un único store lo que hace mas fácil el proceso de depuración.
2. Solo Lectura:
   La única forma de modificar el estado es emitiendo una acción, un objeto que describe que ocurrió.
3. Los cambios se realizan con funciones puras:
   Los reducers son funciones puras que toman el estado anterior y una acción, y devuelven un nuevo estado.
   Funcionalidad

4. Exjemplo:
   - Tienes tu vista (UI)
   - Tu vista va a enviar una acción
   - Tu acción va a llamar un reducer
   - Tu reducer va a llamar a tu store
   - Tu store va a actualizar el estado
   - El estado va a actualizar tu vista (interfaz/UI)

## Store

El Store se importa de la siguiente froma:

```javascript
import { createStorage } from "redux";
```

- **Reducer** => Función pura que retorna el próximo estado.
- **PreloadState / InitialState** => Es el estado inicial de la aplicación, la primera carga, el llamado a una data. Puede ser cualquier tipo de dato.
- **Enhancer** => Función que puede extender redux con capacidades añadidas por librerías externas. Es opcional. Ej. Añadir las dev-tools

Además, debemos crear una constante que será la que utilicemos en la aplicación.

```javascript
const store = createStore(reducer, initialState, enhancer);
```

_PD:_ El Arrow Function de `state` puede quedar en `state => state`

### Datos Importantes del Store

- Contiene el estado de la aplicación.
- Se puede acceder al estado con el método `getState()`
- Se puede actualizar el estado con el método `dispatch(action)`
- Escucha cambios con el método `**subscribe(listener)`
- Deja de escuchar cambios retornando la función del método `subscribe(listener)`

### Configuración Inicial del Store para las Redux DevTools

```javascript
const store = createStore(
  state => state,
  initialState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
```

## Actions

- Las acciones se utilizan con el método `dispatch()`
- Son la única fuente de información del store. Solamente de esa forma el store puede saber que los states cambian.
- Son objetos planos

```javascript
store.dispatch({
  type: "ADD_SONG",
  payload: "Despacito" // Optional
});
```

## Reducers

- Es una función pura.
- Puede haber más de un reducer en una aplicación pero solo debe haber un store.
- Devuelve el siguiente estado.

### ¿Qué es una función pura?

“Dados los mismos parámetros/argumentos/entradas deben retornar el mismo resultado, sin importar el número de veces que se llame”

### ¿Qué no hacer en un Reducer?

- Modificar sus argumentos
- Realizar tareas con efectos secundarios como llamados APIs
- Llamar funciones **no puras** como `Date.now()` `Math.random()`

```javascript
const reducer = function(state, action) {
  // Que hago con el estado y la accion
};
```

Por convención se usa un `switch` para validar si hay algún cambio en el state

```javascript
const reducer = function(state, action) {
    switch(action.type){
        case: "ADD":
            return [...state, {title: action.payload}]
        default:
            return state
    }
};
```

## Subscribe

- Aquí se invoca la actualización de la **UI**

```javascript
store.susbscribe(handleChange);
```

## Ejemplo de Flujo de Eventos con Redux

- Se establece un State (estado) inicial con el que se crea originalmente el store mediante el método `createStore` de redux
- El state inicial define los contenidos que se muestran en la UI mediante `store.getState()` y la función `render()`
- Mediante los elementos de la UI se dispara un evento que se captura mediante un listener (como onsubmit) y que invoca al `store.dispatch()` con una acción específica …
- El Reducer recibe la acción (type y payload) enviada mediante el `store.dispatch()` y genera un nuevo estado que remplazará al estado inicial (o anterior)
- El cambio del estado es detectado por el store y se ejecuta `store.subscribe(handle)` … con la función (handle) que manejará lo que sucede cuando ha cambiado el estado
- En la función (handle) recibida por `store.subscribe(handle)` se invoca la actualización de la UI a partir del nuevo estado creado
- Queda establecido el nuevo estado (state) y queda de nuevo atento el evento (listener) en la UI
